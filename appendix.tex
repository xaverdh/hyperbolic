\appendix

\section{First Order Differential Inequalites}

We prove a small lemma on how a differential inequality translates into bounds against the solutions of the corresponding ordinary differential equation.

\begin{lem}\label{diffineqeq}
  Assume that a smooth function $f \colon I \rightarrow \mathbb{R}$ on some open interval $I = (a,b)$ obeys
  \begin{align*}
    f'(t) \geq (\phi \circ f) (t)
  \end{align*}
  for all $t \in I$, with $\phi \colon \img(f) \rightarrow \mathbb{R}$ smooth and strictly positive. Also let $\psi$ be an antiderivative of $\frac{1}{\phi}$. \\

  Then $\psi(f(t)) \geq \psi(f(a)) + t - a$.

  In particular if $\psi$ is actually injective and therefore invertible, then

  $f(t) \geq \psi^{-1}(t + \psi(f(a)) - a)$ with the right hand side being precisely the solution of the corresponding ODE.

\end{lem}

\begin{proof}

From $\psi' = \frac{1}{\phi}$, we see that $\psi'(f(s)) \geq \frac{1}{f'(s)}$ and compute
\begin{align*}
  \psi(f(t)) - \psi(f(a)) &= \int_{a}^{t} (\psi  \circ f)'(s) ~ds = \int_{a}^{t} \psi'(f(s)) \cdot f'(s) ~ds \\
  &\geq \int_{a}^{t} 1 ~ds = t - a
\end{align*}

\end{proof}

\section{Harmonic Analysis}

\subsection{Mean Values and the Laplacian}

\newcommand{\inner}[2]{\langle #1, #2 \rangle}

For $M \subset \mathbb{R}^n$ a set of finite measure $|M|$ and a function $f$ (integrable on $M$) let $\dashint_{M} f \coloneqq \frac{1}{|M|} \cdot \int_{M} f$ denote the mean value integral of $f$.
Also denote by $B_r(p)$ and $S_r(p) = \partial B_r(p)$ the ball (respectively sphere) of radius $r$ around $p \in \mathbb{R}^n$.\\

For $f \in C^2(\mathbb{R}^n,\mathbb{R})$ we have the following facts on $\Delta f \coloneqq \sum_{i=1}^{n} \partial_i^2 f$:

\begin{align}
  & \dashint_{B_r(p)} \Delta f = \frac{n}{r} \cdot \left.\frac{d}{dt}\right|_{t=r} \dashint_{S_t(p)} f &\\
  & \dashint_{S_t(p)} f = f(p) + \int_{0}^{t} \frac{r}{n} \cdot \dashint_{B_r(p)} \Delta f ~dr &\\
  & \dashint_{B_r(p)} f = f(p) + r^{-n} \int_{0}^{r} t^{n-1} \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta f ~d\alpha ~dt &
\end{align}

The first one is a consequence of Stokes theorem. Fix $p \in \mathbb{R}^n$ and for $q \in S_1$ denote by $\gamma_q \colon [0,\infty) \rightarrow \mathbb{R}^n$ the curve $t \mapsto p + (q-p) \cdot t$ and note that for fixed $t$ the map $q \mapsto \gamma_{q}(t)$ yields a diffeomorphism $S_1 \rightarrow S_t(p)$. We then have:

\begin{align*}
  \int_{B_r(p)} \Delta f
  &= \int_{S_r(p)} \inner{\nabla f}{n}(q) ~dq \\
  &= \int_{S_1} r^{n-1} \inner{\nabla f(\gamma_q(r))}{n(\gamma_q(r))} ~dq \\
  &= r^{n-1} \int_{S_1} \left.\frac{d}{dt}\right|_{t=r} f \circ \gamma_q(t) ~dq
  = r^{n-1} \left.\frac{d}{dt}\right|_{t=r}  \int_{S_1} f \circ \gamma_q(t) ~dq \\
  &= r^{n-1} \left.\frac{d}{dt}\right|_{t=r} \frac{1}{t^{n-1}} \int_{S_t} f
  = \left.\frac{d}{dt}\right|_{t=r} \frac{|S_r|}{|S_t|} \cdot \int_{S_t} f \\
  \dashint_{B_r(p)} \Delta f &= \frac{|S_r|}{|B_r|} \cdot \left.\frac{d}{dt}\right|_{t=r} \dashint_{S_t(p)} f = \frac{n}{r} \cdot \left.\frac{d}{dt}\right|_{t=r} \dashint_{S_t(p)} f
\end{align*}

From this the second one follows by integration, and the third one by combining the two:
\begin{align*}
  \int_{B_r(p)} f &= \int_{0}^{r} \int_{S_t(p)} f ~dt
  = \int_{0}^{r} |S_t| \cdot \dashint_{S_t} f ~dt \\
  &= \int_{0}^{r} |S_t| \cdot \left( f(p) + \int_{0}^{t} \frac{\alpha}{n} \cdot \dashint_{B_\alpha(p)} \Delta f ~d\alpha \right) ~dt \\
  \dashint_{B_r(p)} f &= f(p) + \int_{0}^{r} \frac{|S_t|}{n \cdot |B_r|} \int_{0}^{t} \alpha \dashint_{B_\alpha(p)} \Delta f ~d\alpha  ~dt
\end{align*}


\subsection{Subharmonic functions}

A function $f \in C^2(\mathbb{R}^n,\mathbb{R})$ is called subharmonic, if $\Delta f \geq 0$.

From our results in the previous section, we immediately see that for such a function

\begin{align*}
  \dashint_{B_r(p)} f = f(p) + r^{-n} \int_{0}^{r} t^{n-1} \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta f ~d\alpha ~dt \geq f(p)
\end{align*}

More interesting however is the following result specific to dimension 2:

\begin{lem}\label{subharmbounded}
  Any subharmonic function $f$ defined on all $\mathbb{R}^2$ is either constant or unbounded from above.
\end{lem}

\begin{proof}

  If $\Delta f = 0$ everywhere, then the function is harmonic and it is well known that it must be either constant or unbounded. If $\Delta f(p) > 0$ for some $p \in \mathbb{R}^2$, and then $\Delta f(p) \geq \delta > 0$ on some small ball $B_\epsilon(p)$ around that point. Now for $r \geq \epsilon$ we have $\int_{B_r(p)} \Delta f \geq \int_{B_\epsilon(p)} \delta = \delta \cdot |B_\epsilon|$. We compute

\begin{align*}
  \dashint_{B_r(p)} f &= f(p) + r^{-2} \int_{0}^{r} t \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta f ~d\alpha ~dt \\
  &\geq f(p) + r^{-2} \int_{\epsilon}^{r} t \int_{\epsilon}^{t} \frac{\alpha}{|B_\alpha|} \cdot \int_{B_\alpha(p)} \Delta f ~d\alpha ~dt \\
  &\geq f(p) + r^{-2} \int_{\epsilon}^{r} t \int_{\epsilon}^{t} \delta \cdot \alpha \cdot \frac{\epsilon^2}{\alpha^2} ~d\alpha ~dt \\
  &= f(p) + r^{-2} \int_{\epsilon}^{r} t \left( \operatorname{log}(t) - \operatorname{log}(\epsilon) \right) \cdot \delta \epsilon^2 \\
  &= f(p) + r^{-2} \delta \epsilon^2 \left( \frac{r^2}{2} \left( \operatorname{log}(r) -\operatorname{log}(\epsilon) \right) - \int_{\epsilon}^{r} \frac{t^2}{2} \frac{1}{t} ~dt \right) \\
  &= f(p) + \frac{\delta}{2} \epsilon^2 \left( \operatorname{log}(r) -\operatorname{log}(\epsilon) - \frac{1}{2} \left(1 - \left(\frac{\epsilon}{r}\right)^{2} \right) \right)
\end{align*}

which is clearly unbounded in $r$. Now if we had an upper bound $f \leq c$, then $\dashint_{B_r(p)} f \leq c$ as well, proving the claim.

\end{proof}

This result from \cref{subharmbounded} does not extend to higher dimensions, as the following example shows:

\begin{example}
  For $n \geq 3$, so $k \coloneqq n - 2 \geq 1$
  \begin{align*}
    &f \colon \mathbb{R}^n \rightarrow \mathbb{R},~x \mapsto \phi(|x|) \\
    & \phi \colon \mathbb{R}_{\geq 0} \rightarrow \mathbb{R},~r \mapsto e^{1-r^{-k}}
  \end{align*}
  is a bounded subharmonic function which is not constant.

\end{example}

\begin{proof}
  In detail we have
  \begin{align*}
    \Delta f(x) &= \phi''(|x|) + \phi'(|x|) \cdot \frac{n-1}{|x|}
  \end{align*}
  and our goal is to find a bounded function $\phi \colon \mathbb{R}_{\geq 0} \rightarrow \mathbb{R}$ such that this is always nonnegative. Its jet at $0$ should also vanish, to make $f$ differentiable at the origin, and ensure $\Delta f(0) = 0$.\\

  We first apply a transform \label{transformgamma} $\gamma \colon \mathbb{R}_{\geq 0} \rightarrow (-\infty,1),~ t \mapsto (1-t)^{-1/k}$ with inverse $t \mapsto 1-t^{-k}$, to obtain a simpler form of the differential inequality.

  For a function $\tau : (-\infty,1) \rightarrow \mathbb{R}$ the following is equivalent:
  \begin{itemize}
    \item $\tau'' \geq 0$
    \item $\phi \colon \mathbb{R}_{\geq 0} \rightarrow \mathbb{R},~ \phi(t) \coloneqq \tau(1-t^{-k})$ obeys $\phi''(t) + \phi'(t) \cdot \frac{n-1}{t} \geq 0$
  \end{itemize}

  To see this, we compute
  \begin{align*}
    &\phi'(t) = \tau'(1-t^{-k}) \cdot k \cdot t^{-k-1} \\
    &\phi''(t) = \tau''(1-t^{-k}) \cdot k^2 \cdot t^{-2k-2} - \tau'(1-t^{-k}) \cdot k (k+1) \cdot t^{-k-2}
  \end{align*}
  \begin{align*}
    \phi''(t) + \phi'(t) \cdot \frac{k+1}{t} &= \tau''(1-t^{-k}) \cdot k^2 \cdot t^{-2k-2} - \tau'(1-t^{-k}) \cdot k (k+1) \cdot t^{-k-2} \\
    &+ \tau'(1-t^{-k}) \cdot k \frac{k+1}{t} \cdot t^{-k-1} \\
    &= \tau''(1-t^{-k}) \cdot k^2 \cdot t^{-2k-2}
  \end{align*}
  from which the claim immediately follows.

  Now $\tau(s) = e^s$ is a function which has $\tau'' \geq 0$ and in addition its jet at $-\infty$ vanishes, which causes the jet of $\phi$ to vanish at $0$, as required. It is also bounded on $(-\infty,1)$, so $\phi$ and therefore $f$ will be bounded.

\end{proof}

