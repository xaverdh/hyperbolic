\section{Brody Hyperbolicity of Spaces with Negative Holomorphic Curvature Bound} \label{SecBrody}

Let $(X,h)$ be a Hermitian manifold with $K_h \leq - \epsilon < 0$.
Then according to \cref{negativecurvaturebound}, $X$ will be Kobayashi hyperbolic and therefore Brody hyperbolic.
Consequently, holomorphic maps $\mathbb{C} \to X$ must be constant.
For the special case of holomorphic immersions, we will give an alternative, direct proof of this fact.
We will show that no holomorphic immersion $\mathbb{C} \to X$ exists, given a bound on the holomorphic section curvature of $(X,h)$ as above.

\subsection{Reduction to the Complex Plane}

Let $(X,h)$ be as stated, $f \colon \mathbb{C} \to X$ a holomorphic immersion.
Pull back $h$ via $f$ to get a hermitian metric on the complex plane.
Since $f$ is holomorphic, this metric is compatible with the standard holomorphic structure on $\mathbb{C}$.
Due to \cref{curvatureboundimplieshyperbolic} we have a bound on the Gauß Curvature $K_p$ of this metric at any point $p \in \mathbb{C}$:
\[
  K_p \leq \sup_{v \in T_{f(p)}X } K_h(v) \leq - \epsilon
\]
We obtain a uniform bound on the Gauß curvature of $f^{*}h$, which reduces the statement to showing that there is no such Hermitian metric on $\mathbb{C}$.

\subsection{No Negative Curvature on the Complex Plane}

Consider the complex plane $\mathbb{C}$ with its standard complex structure
\[
  J \coloneqq (\cdot i) \in \operatorname{End}(T\mathbb{C})
\]
and a compatible Hermitian metric $h$, viewed as a map $\hat{h} \colon T\mathbb{C} \rightarrow \overline{T\mathbb{C}}^{*}$, along with its underlying Riemannian metric $g = \Real(h)$, viewed as $\hat{g} \colon T\mathbb{C} \rightarrow \dual{T\mathbb{C}}$.
Moreover, let $g_{st}$ and $h_{st}$ be the standard Riemannian and Hermitian metric on $\mathbb{C}$, which are both also compatible with $J$.
Then $\alpha \coloneqq \hat{h}_{st}^{-1} \circ \hat{h} \in \operatorname{End(T\mathbb{C})}$ commutes with $J$:
\[
  \alpha \circ J = \hat{h}_{st}^{-1} \circ J^{-1*} \circ h = J \circ \hat{h}_{st}^{-1} \circ \hat{h} = J \circ \alpha
\]
Therefore $\alpha_p$ is given by multiplication with some smooth map $\psi \colon \mathbb{C} \rightarrow \mathbb{C}$ and $\hat{h} = \hat{h}_{st} \circ \alpha = \psi \cdot \hat{h}_{st}$.
Since both $h(v,v)$ and $h_{st}(v,v)$ are real valued and positive for some $v \neq 0$, we see that $\psi$ actually takes values in $\mathbb{R}_{>0}$ and we may write $\psi = e^{\sigma}$ for some smooth map $\sigma \colon \mathbb{C} \rightarrow \mathbb{R}$.
By taking real parts we obtain $g = \psi \cdot g_{st}$, so $g$ is conformally equivalent to $g_{st}$ and its Gauss curvature can be written as

\[
  K = - \frac{1}{2} \Delta \sigma \cdot e^{-\sigma}.
\]

This formula can easily be derived from the well known expression (see e.g. \cite[Chap.5,Exercise 3c),p.97]{DoCarmoDiffForms})
\[
  K = - \frac{1}{\sqrt{g_{11} \cdot g_{22}}} \cdot \left[ \frac{\partial}{\partial x_1} \left(\frac{1}{\sqrt{g_{11}}} \cdot \frac{\partial}{\partial x_1} \sqrt{g_{22}} \right) + \frac{\partial}{\partial x_2} \left( \frac{1}{\sqrt{g_{22}}} \cdot \frac{\partial}{\partial x_2} \sqrt{g_{11}} \right) \right]
\]
for the Gauss curvature in an orthogonal parametrization $f \colon U \subset \mathbb{R}^2 \rightarrow \mathbb{R}^2$. Here $g_{ij},~ i,j \in 1, \dots 2$ are the coefficients of the Riemannian metric in the chosen parametrization. In particular
\begin{align*}
  g_{11} &= g(\frac{\partial f}{\partial x_1},\frac{\partial f}{\partial x_1}) \\
  g_{22} &= g(\frac{\partial f}{\partial x_2},\frac{\partial f}{\partial x_2}).
\end{align*}
We choose the identity as parametrization, which is certainly orthogonal with respect to the standard metric, and thefore also for $g$. Using $g = e^{\sigma} g_{st}$ we obtain
\begin{align*}
  g_{11} &= g_{22} = e^{\sigma} \\
  K &= - e^{-\sigma} \cdot \left[ \frac{\partial}{\partial x_1} \left( e^{- \frac{1}{2} \sigma} \cdot \frac{1}{2} \cdot e^{\frac{1}{2} \sigma} \cdot \frac{\partial \sigma}{\partial x_1} \right) + \frac{\partial}{\partial x_2} \left( e^{- \frac{1}{2} \sigma} \cdot \frac{1}{2} \cdot e^{\frac{1}{2} \sigma} \cdot \frac{\partial \sigma}{\partial x_2} \right) \right] \\
  &= - \frac{1}{2} e^{-\sigma} \Delta \sigma
\end{align*}


We claim that the Gauss curvature of $g$ can not be uniformly bounded from above by a negative constant $-\epsilon < 0$.
If it was, we would have $\Delta \sigma \geq 2 \epsilon \cdot e^\sigma$, an inequality which $\sigma$ can not satisfy, as we will now show.

\begin{thm}\label{nonegcurvature}
For $\epsilon > 0$ there is no smooth function $\sigma \colon \mathbb{C} \rightarrow \mathbb{R}$ with $\Delta \sigma \geq 2\epsilon \cdot e^\sigma$.
\end{thm}

\begin{proof}
  Given $\sigma$ as stated, let $\phi_p \colon \mathbb{R}_{\geq 0} \rightarrow \mathbb{R},~r \mapsto \dashint_{B_r(p)} \sigma - \sigma(p)$.
  We find:
  \begin{align}
    \phi_{p}(r) = r^{-2} \cdot \int_{0}^{r} t \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta \sigma ~d\alpha ~dt \label{eqphi}
  \end{align}
Multiplying by $r^2$ and then successively differentiating both sides, we obtain:
\begin{align*}
  & 2 t \cdot \phi_p(t) + t^2 \cdot \phi_p'(t) = \left.\frac{d}{dr}\right|_{r=t} r^2 \phi_p(r) = t \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta \sigma ~d\alpha \\
  & 2 \cdot \phi_p(t) + t \cdot \phi_p'(t) = \int_{0}^{t} \alpha \cdot \dashint_{B_\alpha(p)} \Delta \sigma ~d\alpha \\
  & 2 \cdot \phi_p'(\alpha) + \phi_p'(\alpha) + \alpha \cdot \phi_p''(\alpha) = \alpha \cdot \dashint_{B_\alpha(p)} \Delta \sigma
\end{align*}
Recall Jensens inequality in the form $\dashint_{B_r(p)} e^\sigma \geq e^{\dashint_{B_r(p)} \sigma}$, which can be found in \cite[Appendix B, Theorem 2]{Evans} for example.
Applying this to the above equations and using our assumption $\Delta \sigma \geq 2 \epsilon \cdot e^\sigma$ yields:
\[
  3 \cdot \phi_p'(\alpha) + \alpha \cdot \phi_p''(\alpha) \geq \alpha \cdot 2\epsilon \cdot e^{\sigma(p) + \phi_p(\alpha)} = \left( 2 \epsilon \cdot e^{\sigma(p)} \right) \cdot \alpha \cdot e^{\phi_p(\alpha)}
\]
Note also that due to \cref{eqphi}, $\phi_p$ is bounded from below by $0$.
  Proving that no such function $\phi_p$ exists will be delegated to the following separate lemma, which should be invoked with $\delta = \epsilon \cdot e^{\sigma(p)}$.
\end{proof}

\begin{lem}
  For any $\delta > 0$ there is no smooth function $\phi \colon \mathbb{R}_{>0} \rightarrow \mathbb{R}$, which obeys
  \[
    3 \cdot \phi'(s) + s \cdot \phi''(s) \geq s \cdot 2\delta \cdot e^{\phi(s)}
  \]
  and is bounded from below by $0$.
\end{lem}

\begin{proof}
  Assume $\phi$ exists, and reparametrize it by\footnote{this transform is of the same class as the one used in \cref{transformgamma}}  $\gamma \colon (-\infty,1) \rightarrow (0,\infty),~t \mapsto (1-t)^{-\frac{1}{2}}$ to obtain
\[
  f \colon (-\infty,1) \rightarrow \mathbb{R},~f \coloneqq \phi \circ \gamma.
\]
  Also note that $\gamma'(t) = \frac{1}{2} (1-t)^{-\frac{3}{2}} = \frac{1}{2} \gamma(t)^3 > 0$. So $\gamma$ is an orientation preserving diffeomorphism.
We compute the second derivative of $f$:
\begin{align*}
  f'(t) &= \phi'(\gamma(t)) \cdot \gamma'(t) \\
  f''(t) &= \phi''(\gamma(t)) \cdot \gamma'(t)^2 + \phi'(\gamma(t)) \cdot \gamma''(t) \\
  &= \phi''(\gamma(t)) \cdot \frac{1}{4} (1-t)^{-\frac{6}{2}} + \phi'(\gamma(t)) \cdot \frac{1}{2} \cdot \frac{3}{2} \cdot (1-t)^{-\frac{5}{2}} \\
  &= \frac{1}{4} \cdot \gamma(t)^5 \cdot \left( \phi''(\gamma(t)) \cdot \gamma(t) + 3 \cdot \phi'(\gamma(t)) \right) \\
  &\geq \frac{1}{4} \cdot \gamma(t)^5 \cdot \gamma(t) \cdot 2\delta \cdot e^{\phi(\gamma(t))} = \frac{\delta}{2} \cdot \gamma(t)^6 \cdot e^{f(t)}
\end{align*}
So we derived:
\begin{align}
  f''(t) \geq \frac{\delta}{2} \cdot \gamma(t)^6 \cdot e^{f(t)} \label{ineqf}
\end{align}
Our goal is to show that any such function actually has a blow up at some point strictly before $t = 1$.

First we state some bounds on $f$ and its derivatives, by "throwing away" the exponential term:

\begin{itemize}
  \item $f \geq 0$ and therefore $f''(t) \geq \frac{\delta}{2} \cdot \gamma(t)^6$
  \item $f'(t) \geq c_1 + \frac{\delta}{4} \cdot \gamma(t)^4$ for some $c_1 \in \mathbb{R}$
  \item $f(t) \geq c_0 + c_1 \cdot t + \frac{\delta}{4} \cdot \gamma(t)^2 $ for some $c_0,c_1 \in \mathbb{R}$
\end{itemize}
The first one follows trivially from $\phi(t) \geq 0$, the others by successive integration, using
\begin{align}
  \frac{d}{dt} \gamma(t)^k = \frac{d}{dt} (1-t)^{-\frac{k}{2}} = \frac{k}{2} \cdot (1-t)^{-\frac{k}{2} -1} =  \frac{k}{2} \cdot \gamma(t)^{k+2} \label{derivgamma}
\end{align}

Next we claim that we may find $t_0 < 1$ such that the following conditions are satisfied:
\begin{itemize}
  \item for any $t \geq t_0 \colon f'(t) \geq 1 \geq 0$
  \item for any $t \geq t_0 \colon f'(t)^2 \geq e^{f(t)}$
  \item $2 \cdot e^{-\frac{1}{2} f(t_0)} < 1 - t_0$
\end{itemize}

\begin{proof}
  From $f'(t) \geq c_1 + \frac{\delta}{4} \gamma(t)^4$ and using that $\gamma(t) \rightarrow \infty$ as $t \rightarrow 1$, we see that by choosing $t_0$ close to $1$ we may achieve the first condition.
  Now for this choice of $t_0$ and $t \geq t_0$, we use $f'(t) \geq 1$ to obtain from \cref{ineqf}
  \begin{align*}
    \frac{d}{dt} \left(  f'(t)^2 - e^{f(t)} \right) &= 2 \cdot f'(t) \cdot f''(t) - f'(t) \cdot e^{f(t)} \\
    &\geq f'(t) \cdot e^{f(t)} \left( \delta \cdot \gamma(t)^6 - 1 \right) \\
    &\geq \delta \cdot \gamma(t)^6 - 1
  \end{align*}
  Therefore for some $c \in \mathbb{R}$ we get
  \begin{align*}
    f'(t)^2 - e^{f(t)} \geq c + \int_{0}^{t} \left( \delta \cdot \gamma(t)^6 - 1 \right)
    \overset{\cref{derivgamma}}{=} c + \frac{\delta}{2} \left( \gamma(t)^4 - 1 \right) - t
  \end{align*}
  Again for $t \rightarrow 1$ the right hand side diverges, so by potentially choosing $t_0$ yet closer to $1$, we can arrange for the second condition to hold.

  Lastly from $f(t) \geq c_0 + c_1 \cdot t + \frac{\delta}{4} \gamma(t)^2$ and $t \geq t_0$ we obtain the estimate
\begin{align*}
  e^{\frac{1}{2} f(t)}
    \geq e^{\frac{1}{2} \left( c_0 + c_1 \cdot t \right)} \cdot e^{\frac{\delta}{8} \left( 1 - t \right)^{-1}}
    \geq c \cdot e^{\frac{\delta}{8} \left( 1 - t \right)^{-1}}
\end{align*}
for some constant $c > 0$. Then
\[
  \frac{2}{1-t} \cdot e^{-\frac{1}{2} f(t_0)} \leq \frac{2c}{1-t} \cdot e^{-\frac{\delta}{8} \left( 1 - t \right)^{-1}}
\]
approaches $0$ as $t \rightarrow 1$ so by passing to a new $t_0$ yet closer to $1$, we can make it satisfy the last condition as well.
\end{proof}

Armed with this initial data at $t_0$, we can now put an estimate on our function $f$. For $t \geq t_0$ it obeys $f'(t)^2 \geq e^{f(t)}$ i.e.
\[
  f'(t) \geq e^{\frac{1}{2} f(t)}
\]
and we may bound $f$ by the (locally unique) solution of the corresponding differential equation.
For this we invoke \cref{diffineqeq}. In our situation using the notation from that lemma we have $\phi(s) = e^{\frac{s}{2}}$ and $\psi(s) = - 2 \cdot e^{-\frac{1}{2}s}$, so

\[
  f(t) \geq - 2 \operatorname{log}\left( -\frac{1}{2}(t -2 \cdot e^{-\frac{1}{2}f(t_0)} - t_0) \right) = 2 \cdot \operatorname{log}\left( \frac{2}{b - t} \right)
\]

with $b = t_0 + 2 \cdot e^{-\frac{1}{2} f(t_0)} < 1$. This obviously diverges as $t \rightarrow b$, concluding the proof.
\end{proof}

Further remarks:
\begin{itemize}
  \item The domain of definition of $g$ and $\sigma$ can of course not be shrunk, because any such domain would be conformally equivalent to a disk (in the simply connected case) or covered by one, and therefore carries a compatible hyperbolic metric.
  \item This result can be seen as a negative curvature analogue of the following:
    The Bonnet–Myers theorem (see \cite[chap.9, \S 3, p.200]{DoCarmoRiemannian} for a reference) for Riemannian manifolds with uniform positive curvature bounds implies that any such space must be compact.
    In particular, the complex plane does not carry a metric with uniformly positive Gaussian curvature.
  \item Also similar to the positive curvature situation, the uniformity of the curvature bound entering into $\cref{nonegcurvature}$ can not be relaxed, as we will see in the example below.
\end{itemize}

\begin{example}

Take a strictly subharmonic function $\sigma$ defined on all of $\mathbb{C}$, such as
\[
  \sigma(z) = \operatorname{log}(1+|z|^2)
\]
which has
\[
  \Delta \sigma(z) = 4 \cdot (1+|z|^2)^{-2} > 0
\]
Then
\[
  g(z) \coloneqq e^{\sigma(z)} \cdot g_{st} = \left( 1 + |z|^2 \right) \cdot g_{st}
\]
is a metric on $\mathbb{C}$ with everywhere negative Gaussian curvature
\[
  K_g(z) = -\frac{1}{2} \cdot \Delta \sigma \cdot e^{\sigma} = - 2 \cdot (1+|z|^2)^{-1}.
\]
But observe that $K_g(z) \rightarrow 0$ as $z \rightarrow \infty$, as it should. \\

\end{example}
