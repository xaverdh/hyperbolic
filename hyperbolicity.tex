\section{Hyperbolicity} \label{SecHyperbolicity}

We will now define the notion of hyperbolicity and introduce relevant concepts.
Important definitions mostly follow Langs Book \cite{Lang}.

\subsection{Royden Length and Kobayashi Distance}
Let $X$ be complex manifold and consider its real tangent space $T_qX$ at $q \in X$, which can be regarded as the space of smooth curves $\gamma \colon I_r \rightarrow X$ from intervals $I_r \coloneqq (-r,r)$ with $\gamma(0) = q$ up to first order contact at $0$ in charts.
And it would be standard to only consider curves starting in $I_1$.
This can always be achieved by composing with a suitable diffeomorphism $\alpha \colon I_1 \rightarrow I_r$ with $\alpha'(0) = 1$ which preserves the tangency data, meaning $(\psi \circ \gamma \circ \alpha)'(0) = (\psi \circ \gamma)'(0)$ for any chart $\psi$ around $q$.
\\

However, elements of $T_qX$ could also be seen as tangency data to holomorphic curves, that is, holomorphic maps $D_r \rightarrow X$ from the disk $D_r \coloneqq \{ z \in \mathbb{C} \colon |z| < r\}$ into $X$ up to first order contact at $0$ in charts.

In detail, any such holomorphic curve $\rho \colon D_r \rightarrow X$ can be restricted to the real line to yield a smooth curve $\gamma \colon I_r \rightarrow X$ with derivative $\gamma'(0) = \rho'(0) \in T_qX$.
Here we write $\rho'(z) \coloneqq d\rho_z \cdot 1$ with $1 \in T_z\mathbb{C}$ in slight abuse of notation.
This yields a map
\begin{align*}
  \set{ [\rho] }{ \rho \colon D_r \rightarrow X \text{ hol. },~ \rho(0) = q } &\longrightarrow T_qX \\
  [\rho] &\longmapsto [\left.\rho\right|_{I_r}]
\end{align*}
where $[\dots]$ denotes first order contact on both sides in the appropriate sense, meaning $[\rho_1] = [\rho_2]$ iff $\rho_1(0) = \rho_2(0)$ and for some, and as a consequence any, chart
\[
  \psi \colon U \subset X \rightarrow V \subset \mathbb{C}^n
\]
with $q \in U$, we have $(\psi \circ \rho_1)'(0) = (\psi \circ \rho_2)'(0)$.

The map is injective because the derivatives of the resulting real curves are precisely the derivatives of the original holomorphic curves.
It is surjective as well, since given some $\gamma \colon I_r \rightarrow X$ with $\gamma(0) = q$ we may extend it to a holomorphic map $D_\epsilon \rightarrow X$ at least for small $\epsilon \leq r$ (this can be done in a chart around $q$).\\

In contrast to the real case, it is \emph{not} possible to normalize the radii of the disks, as there is no biholomorphic map $\rho \colon D_1 \rightarrow D_r$ with $\rho'(0) = 1$.
In fact, if $r < 1$ there does not exist a holomorphic map $\rho \colon D_1 \rightarrow D_r$ with $\rho'(0) = 1$, as can be seen by e.g. composing with the inclusion $D_r \hookrightarrow D_1$ and applying the Schwarz Lemma.
But obviously we can always pass to smaller disks, so a natural question arises:

\begin{quote}
  What is the radius of the largest disk representing a given tangent vector?
\end{quote}

The inverse of this largest radius is captured by the Kobayashi or Royden (semi-) length of the tangent vector, defined as follows.

\begin{defn}[Royden Length]
  \begin{align*}
    |\cdot|_{Roy} \colon T_qX &\longrightarrow \mathbb{R}_{\geq 0} \\
    v &\longmapsto \inf \set{ 1/r }{ v = [\rho], \rho \colon D_r \rightarrow X}
  \end{align*}
\end{defn}

Equivalently, the Royden length of a tangent vector can be rewritten as
\[
  |v|_{Roy} = \inf \set{ 1/r }{ r \cdot v = f'(0), f \colon D_1 \rightarrow X}
\]
by composing the holomorphic curves with a rescaling $D_1 \rightarrow D_r$.
We also denote $|\cdot|_{Roy}$ on the space $X$ by $|\cdot|_{X}$.
The Royden length induces a notion of distance between points, the \emph{Kobayashi distance}.

\begin{defn}[Kobayashi Distance]
  \begin{align*}
    d_{Kob} \colon X \times X &\longrightarrow \mathbb{R}_{\geq 0} \\
    (x,y) &\longmapsto \operatorname{inf}\set{L_{Kob}(\gamma)}{ \gamma \colon [0,1] \rightarrow X \text{ piecewise } C^1 \\
    & \hspace{4em} \text{ with } \gamma(0) = x,~ \gamma(1) = y }
  \end{align*}
  where
  \(
    L_{Kob}(\gamma) \coloneqq \int_{a}^{b}\left.|\gamma'(s)\right|_{Roy} ~ds
  \)
  for any piecewise $C^1$ path $\gamma \colon [a,b] \rightarrow X$.
\end{defn}

Again, we write $d_X \coloneqq d_{Kob}$ on the space $X$.
Next we list some properties of $|\cdot|_{Roy}$ and $d_{Kob}$.
Proofs for these can be found in \cite{Lang}.

\begin{itemize}
  \item In the language of Lang, $|\cdot|_{Roy}$ is a semi length function, meaning that it is upper semi-continuous and for all $c \in \mathbb{C}$ we have $|c \cdot v|_{Roy}| = |c| \cdot |v|_{Roy}$ where $|c|$ denotes the absolute value of $c$.
  \item For any holomorphic map $f \colon X \rightarrow Y$ and all $v \in T_pX$ we have that
  \[
    |df_p \cdot v|_{Y} \leq |v|_X.
\]
  This could serve as an alternative definition of the Royden length, in that it is the largest semi length function on the space $Y$ such that this holds for all spaces $X$ (and actually it suffices to demand this for $X=D_1$ only).
\end{itemize}

Neither the Royden length nor the Kobayashi distance are nondegenerate in general.
For the case of the complex plane, we have the following.

\begin{example}
  On $X = \mathbb{C}$ both $|\cdot|_X$ and $d_X$ vanish.

  The reason for this is that the complex plane admits embeddings of arbitrarily large disks around any point $p \in \mathbb{C}$ with fixed derivative (say $1$) at $p$.
\end{example}

Now these properties for $d_{Kob}$ hold:
\begin{itemize}
  \item It obeys the triangle inequality (by construction as infimum over paths). Therefore in the language of Lang, $d_{Kob}$ will be a semi distance.
  \item It is continuous with respect to the topology underlying the space.
  \item Holomorphic maps are weakly distance decreasing with respect to $d_{Kob}$.
\end{itemize}

Again the latter property (for all maps with source $X = D_1$) can be used to define $d_{Kob}$, as Lang does for example.

Lastly we give an example that hints at a close connection with hyperbolic geometry (in the classical sense of the word), which we will explain in the following:

\begin{example}
  On $D_1$ the Royden length and the induced Kobayashi distance are those associated with the hyperbolic metric.
\end{example}

\begin{proof}[Proof sketch]
  Pick $q \in D_1$, then the hyperbolic length of $v \in T_qD_1$ is given by $|v|_{hyp} = |v|_{st} \frac{1}{1-|q|^2}$ with $|\cdot|_{st}$ the standard euclidian length, which may be viewed as the absolute value on $T_q\mathbb{C} \overset{\sim}{=} \mathbb{C}$. The biholomorphism
  \[
    D_1 \longrightarrow D_1,~ z \mapsto \frac{q-z}{1+\bar{q}z}
\]
  is an isometry of both $|\cdot|_{Roy}$ (by the contraction property) as well as $|\cdot|_{hyp}$ (by explicit calculation).
  It maps $q$ into $0$, where the hyperbolic length agrees with the euclidean one.
  We will see that the same is true of $|\cdot|_{Roy}$. If $v \in T_0D_1 \overset{\sim}{=} \mathbb{C}$, then the map
  \[
    D_{1/|v|} \longrightarrow D_1,~ z \mapsto v \cdot z
\]
  yields a bound $|v|_{Roy} \leq \frac{1}{1/|v|} = |v|$. Also given any map $f \colon D_r \to D_1$ with $f'(0) = v$, by composing with
  \[
    \phi_r \colon D_1 \rightarrow D_r,~ z \mapsto r \cdot z
\]
  we get a biholomorphism of the disk, which by the Schwarz lemma must have
  \[
    |v| \cdot r = |f'(0)| \cdot r = |(f \circ \phi_r)'(0)| \leq 1.
\]
  So we see that $|v|_{Roy} \geq \frac{1}{r} \geq |v|$.
\end{proof}

\subsection{Classical Holomorphic Sectional Curvature}

We introduce the \emph{Holomorphic Sectional Curvature} as it can be found in standard literature on complex geometry.
The next section will then rephrase this in the language of Lang/Kobayashi.\\

Let $(X,J,h)$ be a Hermitian manifold, that is, a complex manifold $X$ with an almost complex structure $J$ and with a Hermitian metric $h \in \Gamma((T^{1,0}X \otimes T^{0,1}X)^{*})$.
Decomposing the Hermitian metric into its real and imaginary part $h = \Real{h} + i \cdot \Imag{h}$ and pulling back under the holomorphic identifications

\begin{align*}
  \xi \colon (TX,J) &~\tilde{\longrightarrow}~ (T^{1,0}X,i) \\
  v &\longmapsto \frac{1}{2} (v - i \cdot Jv) \\
  \tilde{\xi} \colon (TX,-J) &~\tilde{\longrightarrow}~ (T^{0,1}X,i) \\
  v &\longmapsto \frac{1}{2} (v + i \cdot Jv)
\end{align*}

yields for $u,v \in TX$
\[
  \xi^{*}h(u,v) \coloneqq h(\xi(u),\tilde{\xi}(v)) = g(u,v) - i \cdot \omega(u,v)
\]
where $g$ a Riemannian metric and $\omega \in \Omega^2(X)$ and the usual relation $\omega(x,Jy) = g(x,y)$ for $x,y \in TX$ holds.

Also $h$ may be extended to a bilinear form
\[
  \comp{\xi^{*}h} \in \Gamma((\comp{T}X \otimes \comp{T}X)^{*})
\]
by complexifying $\xi^{*}h$, and this complexification will agree with $h$ when restricted to
\[
  T^{1,0}X \otimes T^{0,1}X \subset \comp{T}X \otimes \comp{T}X.
\]

So in the following we will identify $h$ with $\comp{\xi^{*}h}$ and always assume that $h$ is defined on all of $\comp{T}X \otimes \comp{T}X$.\\


Now consider the Chern connection $\nabla$ (with respect to $h$) with induced exterior derivative
\[
  d_\nabla \colon \Omega^k(\hol{T}X) \rightarrow \Omega^{k+1}(\hol{T}X).
\]
Let $F \in \Omega^{1,1}(\operatorname{End}(\hol{T}X))$ be its curvature form, defined by
\[
  F \wedge \chi = d_\nabla^2 \chi \text{ for all } \chi \in \Omega^0(\hol{T}X) = \Gamma(\hol{T}X).
\]
By the same argument as for the hermitian metric, we may extend $F$ to live on the full complexified tangent space and by slight abuse of notation also denote it $F \in \Omega^{1,1}(\operatorname{End}(\comp{T}X))$.

\begin{remark*}
  Note that if and only if the manifold is Kähler will the Chern connection correspond to the Levi-Civita connection on $(X,g)$ under the identifications $\xi,\tilde{\xi}$.
  See e.g.~\cite[Prop.4.A.\{7,8,9\},p.210]{Huybrechts} for a proof of this.
\end{remark*}

Denote the curvature tensor of $\nabla$ by
\[
  \Theta(v,v',u,u') \coloneqq h(F(u,u')v,v')
\]
where $u,v \in \hol{T}X$ and $u',v' \in \ahol{T}X$.
Again, if the manifold is Kähler, pulling this back under $\xi,\tilde{\xi}$ will give the Riemannian curvature.
Then we can define:

\begin{defn}
  The holomorphic sectional curvature of $h$ is the map
  \begin{align*}
    \operatorname{HolSec}_h \colon \hol{T}X - 0 &\longrightarrow \mathbb{C} \\
    v &\longmapsto \frac{ \Theta(v,\bar{v},v,\bar{v}) }{ \|v\|^4 }
  \end{align*}
\end{defn}

This is in fact real valued, as can be seen e.g. locally in charts.
Pulling back by $\xi$ we can rewrite it as
\begin{align*}
  &K_h \colon TX - 0 \longrightarrow \mathbb{R} \\
  &K_h(v) \coloneqq \operatorname{HolSec}_h(\xi(v)) = \frac{h(F(v,Jv)Jv,v)}{\|v\|^4}
\end{align*}
The expression can be thought of as the "curvature" associated with the plane spanned by $v$ and $Jv$. Indeed:

\begin{lem}
  If $X$ is Kähler then
  \[
    K_h(v) = \frac{g(R(v,Jv)Jv,v)}{\|v\|^4} = K_g(v,Jv)
\]
  where $R$ is the Riemannian curvature endomorphism and $K_g(a,b)$ the Riemannian sectional curvature of $g$.
\end{lem}
\begin{proof}
  Let $v \in TX - 0$.
  We compute:
  \begin{align*}
    F(\xi(v),\tilde{\xi}(v)) &= \frac{1}{4} F(v - iJv,v + iJv) \\
    &= \frac{1}{4} \left( F(v,v) + F(Jv,Jv) + i F(Jv,v) - i F(v,Jv) \right) \\
    &= \frac{i}{4} \left( F(Jv,v) - F(v,Jv) \right) = -\frac{i}{2} F(v,Jv) \\
    K_h(v) &= h(F(\xi(v),\tilde{\xi}(v)) \cdot \xi(v),\tilde{\xi}(v)) \\
    &= \frac{1}{4} h(-\frac{i}{2} F(v,Jv) \cdot (v -iJv),v+iJv) \\
    &= -\frac{i}{8} \Big[ h(F(v,Jv)v,v) + h(F(v,Jv)Jv,Jv) \\
    &\hspace{3em}+ i h(F(v,Jv)v,Jv) - i h(F(v,Jv)Jv,v) \Big] \\
    &= \frac{1}{4} h(F(v,Jv)v,Jv)
  \end{align*}
  We used antisymmetry of $F$ both in the form part and in the endomorphism part.
  Observe that
  \[
    \|\xi(v)\| = \frac{1}{2} \cdot {(\|v\|^2 + \|Jv\|^2)}^{\frac{1}{2}} = \frac{1}{\sqrt 2} \|v\|
  \]
  so $\|\xi(v)\|^4 = \|v\|^4 \cdot \frac{1}{4}$ and we conclude
  \[
    K_h(v) = \frac{h(F(v,Jv)Jv,v)}{\|v\|^4}
  \]
  Since $X$ is Kähler, we may replace $F$ with the Riemannian data $R$ and since we know that $K_h$ must be real, we may swap $h$ for its real part $g$ as well. Then the claim follows.
\end{proof}


\subsection{Ricci Function / Holomorphic Sectional Curvature}

Following Lang \cite[Theorem 3.4, chap.V, p.144]{Lang}, there is a different perspective on the holomorphic sectional curvature.
Let $(X,h)$ be a hermitian manifold, then we have:
\begin{thm}\label{curvatureboundimplieshyperbolic}
  For $v \in T_pX - 0$:
  \begin{align*}
    K_h(v) = \operatorname{sup} \set{ K_{f^{*}h}(u)}{&Y \overset{f}{\hookrightarrow} X \text{ hol. embedding with } f(q) = p,\\
    &\exists \lambda \in \mathbb{C} \colon v = \lambda \cdot df_q(u) }
  \end{align*}
\end{thm}
Beware that Lang uses what he calls the Ricci function instead of $K_h$ in his presentation of the theorem, differing from the holomorphic sectional curvature by a sign.

Although the supremum is taken over complex submanifolds of $X$ around $p$, since we are only interested in the local curvature data of these, we can instead take maps from the unit disc. This is how Kobayashi states it in \cite[(2.3.2),p.32]{Kobayashi}:
\begin{thm*}
  For any $v \in T_pX - 0$
  \begin{align*}
    K_h(v) = \operatorname{sup} \set{K_{f^{*}h}(1)}{&D_1 \overset{f}{\rightarrow} X \text{ hol. with } f(0) = p,\\
    &\exists \lambda \in \mathbb{C} \colon v = \lambda \cdot f'(0) }.
  \end{align*}
  where $1 \in T_0 \mathbb{C}$ is your standard tangent vector.
\end{thm*}
Note that on a one dimensional complex manifold, which is automatically Kähler, the holomorphic sectional curvature is just the Gauß curvature.
In particular, $K_{f^{*}h}(1)$ will be the Gauß curvature of the pulled back metric at $0$.\\

For the expression in terms of a supremum over holomorphic curves or over submanifolds to be meaningful, one does in fact not need a true hermitian metric. 
Any length function or even semi-length function $F$ will suffice, and $K_F$ is then defined by the right hand side of these equalities.
The reader is referred to Kobayashi \cite{Kobayashi} for details on this.

%Its trace $\operatorname{tr}(F) \in \Omega^{1,1}(X)$ gives rise to a form of the same type as $\omega$, and $\operatorname{tr}(F)$ will also have an associated hermitian metric given by $h_F(x,y) \coloneqq \operatorname{tr}(F)(x,Jy) + i \operatorname{tr}(F)(x,y)$.

\subsection{Hyperbolic Spaces}

\begin{defn}
  A complex manifold $X$ is called \emph{Kobayashi hyperbolic} or just \emph{hyperbolic} if $d_X$ is a true distance, i.e.
  \begin{equation*}
    d_X(p,q) = 0 \implies p = q.
  \end{equation*}
\end{defn}

If $X$ is hyperbolic and connected then $d_X$ will in fact induce the topology of $X$ \cite[Theorem 2.3, chap.I, p.19]{Lang}.

\begin{defn}
  $X$ will be called \emph{Brody hyperbolic} if and only if every holomorphic map $\mathbb{C} \rightarrow X$ is constant.
\end{defn}

Clearly $X$ being Kobayashi hyperbolic implies that it must be Brody hyperbolic:
Given some holomorphic map $f \colon \mathbb{C} \rightarrow X$ for $p,q \in \mathbb{C}$ we have
\[
  d_X(f(p),f(q)) \leq d_\mathbb{C}(p,q) = 0 \implies f(p) = f(q).
\]
The converse holds for example for compact $X$, see \cite[Theorem 2.1, chap.III, p.68]{Lang}.

\begin{prop}[Hyperbolicity of Coverings]\label{hypcoverings}
  Given a covering $Y \to X$, the space $X$ is Kobayashi hyperbolic if and only if $Y$ is.
\end{prop}
This is \cite[Proposition 2.8, chap.I, p.23]{Lang} in Lang, where the reader is referred to for a proof.

\begin{lem}[Hyperbolic Riemann Surfaces]\label{hypsurfaces}
  In dimension $1$ the connected hyperbolic Riemann surfaces are precisely those that also naturally carry a hyperbolic metric (i.e. that are covered by the unit disc).
\end{lem}
\begin{proof}
  By the classification of Riemann surfaces, the universal covering $Y \to X$ of the space $X$ is either a disc, the complex plane or the Riemann sphere.

  The complex plane trivially admits a nonvanishing holomorphic map into it, and the Riemann sphere as well, since the complex plane embeds via one of the standard charts.
  Composing with the covering map yields a nonconstant map into $X$.
  So in those cases $X$ is not Brody hyperbolic, so certainly not hyperbolic in the (stronger) Kobayashi sense.

  We are left with the case where $Y$ is a disc, which is certainly Kobayashi hyperbolic since the Kobayashi distance agrees with the standard hyperbolic metric on the disc.
  By \cref{hypcoverings} this property will then be inherited by $X$.
\end{proof}

So in particular surfaces of higher ($\geq 1$) genus will be hyperbolic.

This suggests a close connection with the curvature of the space, and indeed there is the following theorem by Kobayashi \cite[(3.7.1), p.112]{Kobayashi}:
\begin{thm}\label{negativecurvaturebound}
  Let $X$ be a complex space (e.g. a complex manifold).
  If there is a length function $F$ with holomorphic sectional curvature $K_F$ bounded from above by a negative constant, then X is Kobayashi hyperbolic.
\end{thm}

The special case of interest is that $F$ is the length function induced by a hermitian metric $h$ on $X$. Then $K_F = K_h$ as defined here.

In the following, we will try to connect the bound on holomorphic sectional curvature in this theorem with real Riemannian geometry in various ways.
