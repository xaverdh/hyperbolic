\section{Hyperbolicity of Complexifications}

\subsection{Complexifications}

\begin{defn}
  Given a real analytic manifold $M$, a \emph{complexification} of $M$ is an embedding $\iota \colon M \hookrightarrow X$ of $M$ as a totally real submanifold of a complex manifold $X$.
  Here, \emph{totally real} means that for every point $p \in M$ there are charts $\phi \colon U \subset M \rightarrow \mathbb{R}^n$ and $\psi \colon V \subset X \rightarrow \mathbb{C}^n$ around $p$ and $\iota(p) \in X$, such that
  \begin{center}
    \begin{tikzcd}
      U \subset M \arrow[r,"{\iota}",hook] \arrow[d, "{\phi}"]
      & V \subset X \arrow[d, "{\psi}"] \\
      \mathbb{R}^n \arrow[r, hook]
      & \mathbb{C}^n
    \end{tikzcd}
  \end{center}
  is a pullback diagram, where the lower arrow is the set theoretic inclusion of $\mathbb{R}^n$ into $\mathbb{C}^n$.
  In particular, $\psi^{-1}(\mathbb{R}^n) = \iota(U)$.
\end{defn}

A complexification of a given real analytic manifold $M$ always exists and is unique in the following sense:
For any two complexifications there are neighbourhoods of $M$ in the respective complexifications that are biholomorphic via a map that commutes with the inclusions of $M$.
These results are originally due to Bruhat and Whitney \cite{BruhatWhitney}.


\begin{defn}
  Let $(M,g)$ be a real analytic Riemannian manifold.
  We will say that a complexification $\iota \colon M \hookrightarrow X$ is of \emph{Stenzel type} if $X$ is an open neighbourhood of the zero section in $T^{*}M$, the map $\iota$ is the embedding of $M$ as zero section of the bundle $T^{*}M \overset{\pi}{\rightarrow} M$, and the following conditions are satisfied (on $X$):
  \begin{itemize}
    \item The tautological one-form $\alpha$ of $T^{*}M$ is given by $\alpha = - \operatorname{Im}(\overline{\partial} \phi)$, where $\phi(p) = g_{\pi(p)}(p,p)$
    \item $\sigma \colon T^{*}M \rightarrow T^{*}M,~ p \mapsto -p$ fulfills $\sigma^{*}J = -J$ with $J$ being the complex structure of $X$
  \end{itemize}
\end{defn}

A Stenzel complexifiation of $(M,g)$ is unique if it exists, and turns $X$ into a Kähler manifold with Kähler form $\omega = d\alpha$, see \cite[Theorem 2.1.1]{Stenzel86}.
Furthermore, the metric associated with $\omega$ restricts to $g$ on $M$, i.e. $\iota$ is an isometry.

The existence of Stenzel complexifications was proven in \cite{Stenzel86} for many types of homogeneous spaces and in more generality, namely for compact base manifolds, in \cite{Stenzel91}.

\subsection{Monge-Ampère Foliation and Implications for Hyperbolicity}

Let $M \hookrightarrow X \subset T^{*}M$ be a Stenzel complexification of $M$ with tautological one-form $\alpha$ and Kähler form $\omega = d\alpha$.
We will examine under what conditions $X$ is hyperbolic.\\

Define a Liouville vector field $\Xi \coloneqq \sharpen{\alpha}{\omega}$.
According to \cite[section 2.4]{Stenzel86}, $\Xi$ and $J\Xi$ span an integrable distribution, the \emph{Monge-Ampère} distribution.
Its leaves are complex manifolds of dimension $1$, which are flat with respect to the Kähler metric $b = \Real(h)$ on $X$, see \cite[Prop. 2.4.6]{Stenzel86}.

Also since $X$ is Kähler, the holomorphic sectional curvature of $b$ is given by
\[
  K_h(v) = \frac{b(R(v,Jv)Jv,v))}{\|v\|^4}
\]
and we see that $K_h(\Xi) = 0$.
This means that the hyperbolicity criterion from \cref{negativecurvaturebound} is always violated in this setting.\\

As we will see, if M is compact and the complexification extends to the whole bundle $X = T^{*}M$, the leaves of the Monge-Ampère distribution yield a nonconstant map from the complex plane into $X$, which is therefore not hyperbolic.
We consider an example of this type next.

\subsection{Complexifications of Spheres}

We consider the tangent bundle of the sphere $S^n$, with its standard round metric $g$ induced from $S^n \subset \mathbb{R}^{n+1}$, and identify it with

\begin{align*}
  V &\coloneqq \set{ z \in \mathbb{C}^{n+1} }{ \sum_{j=0}^{n} z_j^2 = 1 }\\
  &= \set{ x + i y \in \mathbb{C}^{n+1} }{ |x|^2 - |y|^2 = 1,~ \pair{x}{y} = 0 }
\end{align*}

via the map

\begin{align*}
  \rho \colon V &\longrightarrow TS^n \\
  x + i y &\longmapsto \left( \frac{x}{|x|},~ - \frac{y}{|y|} \cdot \log(|x| + |y|) \right)
\end{align*}

Due to $|x| + |y| = (1 + |y|^2)^{\frac{1}{2}} + |y|$ this can also be written

\begin{align*}
  \rho(x+iy)
  = \left( \frac{x}{|x|},~ - \frac{y}{|y|} \cdot \operatorname{arcsinh}(|y|) \right).
\end{align*}

It is in fact a diffeomorphism with inverse given by

\begin{align*}
  \rho^{-1}(x,v)
  = x \cdot \operatorname{cosh}(|v|) - i \cdot \frac{v}{|v|} \cdot \operatorname{sinh}(|v|).
\end{align*}

We endow $V$ with the standard complex structure $J = (\cdot i)$ from $\mathbb{C}^{n+1}$.

Next we define a potential
\begin{align*}
  \phi \colon V &\longrightarrow \mathbb{R} \\
  z = x + iy &\longmapsto g(\rho(z),\rho(z)) = \log(|x| + |y|)^2
\end{align*}

and claim the following for $\hat{\rho} \coloneqq \hat{g} \circ \rho$:

\begin{prop*}
  For $z = x + i y \in V$
  \begin{align*}
    \hat{\rho}^{*} \alpha_z = - \frac{\log(|x| + |y|)}{|x| \cdot |y|} \alpha_{st} = - \frac{1}{2} (d\phi \circ J)_z
  \end{align*}

  where $\alpha \coloneqq \alpha_{T^{*}S^n}$ is the tautological one-form of $T^{*}S^n$ and $\alpha_{st}$ the standard one-form of $\mathbb{C}^{n+1}$ as cotangent bundle of $\mathbb{R}^{n+1}$.

  \begin{proof}
    Denote by $\pi_{TS^n}$ and $\pi_{T^{*}S^n}$ the respective bundle projections. For $z \in V$ and $v \in T_zV \subset T_z \mathbb{C}^{n+1} \overset{\sim}{=} \mathbb{C}^{n+1}$ we compute
    \begin{align*}
      \hat{\rho}^{*} \alpha (v)
      = (\hat{g} \circ \rho)(z) ( d\pi_{T^{*}S^n}(d\hat{g}_{\rho(z)} \cdot d\rho_z v))
      = g(\rho(z), d(\pi_{TS^n} \circ \rho)_z v)
    \end{align*}
    Now with
    \begin{align*}
      &(\pi_{TS^n} \circ \rho)(x +i y) = \frac{x}{|x|} \\
      &d(\pi_{TS^n} \circ \rho)_{x +i y} v = \frac{\Real(v)}{|x|} - \frac{x}{|x|^3} \cdot \sum_{j=0}^{n} x_j \cdot \Real(v_j)
    \end{align*}
    and using $\rho(z) \perp x$ wrt. the standard metric of $S^n \subset \mathbb{R}^{n+1}$ we see that
    \begin{align*}
      (\hat{g} \circ \rho)^{*} \alpha (v)
      &= g(\rho(z), \frac{\Real(v)}{|x|} ) \\
      &= - \frac{\log(|x|+|y|)}{|x| \cdot |y|} \cdot g(y,\Real(v)) \\
      &= - \frac{\log(|x|+|y|)}{|x| \cdot |y|} \cdot \alpha_{st}(v).
    \end{align*}

    Also for $z \in V$ and $v \in T_zV \subset T_z \mathbb{C}^{n+1} \overset{\sim}{=} \mathbb{C}^{n+1}$ we compute
    \begin{align*}
      \frac{1}{2} d\phi \circ J(v)
      &= \frac{\log(|x| + |y|)}{|x|+|y|} \left( \frac{1}{2|x|} d|x|^2(Jv) + \frac{1}{2|y|} d|y|^2 (Jv) \right) \\
      &= \frac{\log(|x| + |y|)}{|x| \cdot |y| \cdot (|x|+|y|)} \frac{1}{2} \left( |y| \cdot d|x|^2 (Jv) + |x| \cdot d|y|^2(Jv) \right).
    \end{align*}

    Note that with $|x|^2 = 1 + |y|^2$ (on V) we have $d|x|^2 = d|y|^2$, so this can be written as
    \begin{align*}
      \frac{\log(|x| + |y|)}{|x| \cdot |y|} \frac{1}{2} \left( d|y|^2(Jv) \right)
    \end{align*}
    and
    \begin{align*}
      &\frac{1}{2} d|y|^2(Jv)
      = \sum_{j=0}^{n} y_j \cdot dy_j(Jv)
      = \sum_{j=0}^{n} y_j \cdot dx_j(v) = \alpha_{st}(v)
    \end{align*}
    from which the claim follows.
  \end{proof}
\end{prop*}

So we give $V$ with the exact symplectic structure
\begin{align*}
  \alpha_V \coloneqq - \frac{\log(|x| + |y|)}{|x| \cdot |y|} \alpha_{st}
\end{align*}

such that $\hat{\rho}$ becomes a symplectomorphism.

Now we claim that

\begin{lem}
  $X \coloneqq T^{*}S^n$ with complex structure $\hat{\rho}_{*}J$ coming from $V$ and $\phi \circ \hat{\rho}^{-1}$ as Kähler potential is a Stenzel complexification of $(S^n,g)$.
  \begin{proof}
    By construction $\phi \circ \hat{\rho}^{-1}$ is the quadratic function induced by $g$.

    Also we have already seen that
    \begin{align*}
      \hat{\rho}^{*} \alpha_{T^{*}S^n} &= - \frac{1}{2} d\phi \circ J \\
      &= - \left( \hat{\rho}^{*} \frac{1}{2} d(\phi \circ \hat{\rho}^{-1}) \right) \circ J \\
      &= \hat{\rho}^{*} \left( - \frac{1}{2} d(\phi \circ \hat{\rho}^{-1}) \circ \hat{\rho}_{*}J \right) \\
      &= \hat{\rho}^{*} \left( - \Imag(\overline{\partial} (\phi \circ \hat{\rho}^{-1})) \right)
    \end{align*}
    so $\alpha_{T^{*}S^n} = - \Imag(\overline{\partial} (\phi \circ \hat{\rho}^{-1}))$.
    Finally
    \begin{align*}
      \sigma \colon V \longrightarrow V, z \longmapsto \overline{z}
    \end{align*}
    when transported to $T^{*}S^n$ via $\hat{\rho}$ corresponds to the standard antisymplectic involution $T^{*}S^n \longrightarrow T^{*}S^n,~ y \longmapsto -y$. It obeys $\sigma^{*} J = - J$, which makes it antiholomorphic as required.
  \end{proof}
\end{lem}

Having established the complex structure in the complexified spheres, we will now investigate its hyperbolicity.

\subsection{Complexifications of Spheres are not Hyperbololic}

For this complexification of $S^n$ we see that:

\begin{lem}\label{complspherenothyperbolic}
  $X = T^{*}S^n$ is not Brody hyperbolic.

  \begin{proof}
    \begin{align*}
      \mathbb{C} \overset{\exp}{\rightarrow} (\mathbb{C} - 0) \overset{f}{\rightarrow} V
    \end{align*}
    is not constant with
    \begin{align*}
      f(w) \coloneqq \left( \frac{1}{2} \left(w + \frac{1}{w} \right), \frac{i}{2} \left(w - \frac{1}{w} \right), 0, \dots, 0 \right).
    \end{align*}
  \end{proof}

\end{lem}

This map was obtained by factoring $1 = z_0^2 + z_1^2 = (z_0 + i z_1) \cdot (z_0 - i z_1)$, and demanding $w = z_0 + i z_1$, $\frac{1}{w} = z_0 -i z_1$. Although we will write down the holomorphic map associated with the Monge-Ampère distribution, this highlights a far easier way to obtain the nonhyperbolicity claim.

\subsubsection{The Monge-Ampère Vector Field for Complexifications of Spheres}

Let us compute the Monge-Ampère vector field $\Xi$ for this setup.
We postulate that $\Xi(x + i y) = x \cdot a(z) + i y \cdot b(z)$ for some real valued functions $a,b \colon V \rightarrow \mathbb{R}$, and would like to determine these coefficient functions.

Denote
\begin{align*}
  \sigma(x + i y) \coloneqq - \frac{\log(|x| + |y|)}{|x| \cdot |y|}
\end{align*}
such that $\alpha_V = \sigma \cdot \alpha_{st}$ then we compute
\begin{align*}
  \ins{\Xi}{d\alpha_V} &= \ins{\Xi}{d (\sigma \cdot \alpha_{st})} = \sigma \cdot \ins{\Xi}{d\alpha_{st}} + \ins{\Xi}{d\sigma} \wedge \alpha_{st} = \sigma \cdot \ins{\Xi}{d\alpha_{st}} + \ins{\Xi}{d\sigma} \cdot \alpha_{st}
\end{align*}
and the last inequality is due to $\ins{\Xi}{\alpha_V} = \omega_V(\Xi,\Xi) = 0$ which implies $\ins{\Xi}{\alpha_{st}} = 0$.

First we note that on $V$ we have $|x|^2 - |y|^2 = 1$ so $d|x|^2 = d|y|^2$ and compute for $d\sigma$:

\begin{align*}
  d\sigma_z &= - \frac{d|x| + d|y|}{|x| \cdot |y| \cdot (|x| + |y|)} + \frac{\log(|x| + |y|)}{|x|^2 \cdot |y|^2} \cdot d(|x| \cdot |y|) \\
  &= - \frac{\frac{1}{2|x|} d|x|^2 + \frac{1}{2|y|}d|y|^2}{|x| \cdot |y| \cdot (|x| + |y|)} + \frac{\log(|x| + |y|)}{|x|^2 \cdot |y|^2} \cdot (|x| \cdot d|y| + |y| \cdot d|x|) \\
  &= - \frac{d|y|^2}{2} \frac{\frac{1}{|x|} + \frac{1}{|y|}}{|x| \cdot |y| \cdot (|x| + |y|)} + \frac{\log(|x| + |y|)}{|x|^2 \cdot |y|^2} \cdot (\frac{|x|}{2|y|} \cdot d|y|^2 + \frac{|y|}{2|x|} \cdot d|x|^2) \\
  &= - \frac{d|y|^2}{2} \frac{|y| + |x|}{|x|^2 \cdot |y|^2 \cdot (|x| + |y|)} + \frac{\log(|x| + |y|)}{|x|^2 \cdot |y|^2} \cdot \frac{d|y|^2}{2} \frac{|x|^2 + |y|^2}{|x| \cdot |y|} \\
  &= - \frac{d|y|^2}{2} \cdot \frac{1}{|x|^2 \cdot |y|^2} \left( 1 - \frac{\log(|x| + |y|)}{|x| \cdot |y|} \cdot |z|^2 \right) \\
  &= - \frac{d|y|^2}{2} \cdot \frac{1}{|x|^2 \cdot |y|^2} \cdot (1 + \sigma(z)\cdot |z|^2 )
\end{align*}

With

\begin{align*}
  \ins{\Xi}{\frac{d|y|^2}{2}} = \sum_{j=0}^{n} y_j dy_j(\Xi) = \sum_{j=0}^{n} y_j^2 \cdot b = |y|^2 \cdot b
\end{align*}

we conclude that

\begin{align*}
  \ins{\Xi}{d\sigma} = - b \cdot \frac{1}{|x|^2} \cdot (1 + \sigma(z)\cdot |z|^2 ).
\end{align*}

Now on $V$ we also have $\pair{x}{y} = 0$, so

\begin{align*}
  \sum_{j=0}^{n} x_j dy_j = - \sum_{j=0}^{n} y_j dx_j
\end{align*}
and therefore
\begin{align*}
  \ins{\Xi}{d\alpha_{st}} &= \sum_{j=0}^{n} \ins{\Xi}{dy_j \wedge dx_j} = \sum_{j=0}^{n} (dy_j(\Xi) \cdot dx_j - dx_j(\Xi) \cdot dy_j) \\
  &= \sum_{j=0}^{n} (y_j \cdot b \cdot dx_j - x_j \cdot a \cdot dy_j) = (b+a) \cdot \alpha_{st}
\end{align*}

Putting things together we find

\begin{align*}
  \ins{\Xi}{d\alpha_V} &= \alpha_{st} \left( \sigma \cdot (a+b) - b \cdot \frac{1}{|x|^2} \cdot (1 + \sigma \cdot |z|^2) \right) \\
  &= \alpha_{st} \left( \sigma \cdot a - b \cdot \frac{1}{|x|^2} \cdot (1 + \sigma\cdot |z|^2 - \sigma \cdot |x|^2) \right) \\
  &= \alpha_{st} \left( \sigma \cdot a - b \cdot \frac{1}{|x|^2} \cdot (1 + \sigma\cdot |y|^2) \right).
\end{align*}

Further using that $\Xi$ is tangent to $V$ we have

\begin{align*}
  |y|^2 \cdot b = \sum_{j=0}^{n} y_j dy_j(\Xi) = \sum_{j=0}^{n} x_j dx_j(\Xi) = |x|^2 \cdot a
\end{align*}

so we may substitute $b = a \cdot \frac{|x|^2}{|y|^2}$ to obtain

\begin{align*}
  \ins{\Xi}{d\alpha_V} &= \alpha_{st} \cdot a \left( \sigma - \frac{1}{|y|^2} \cdot (1 + \sigma\cdot |y|^2) \right) \\
  &= - \alpha_{st} \cdot a \cdot \frac{1}{|y|^2} = \alpha_V
\end{align*}

for the choice

\begin{align*}
  a(z) \coloneqq - \sigma(z) \cdot |y|^2 = \log(|x| + |y|) \cdot \frac{|y|}{|x|}.
\end{align*}
This also confirms that $\Xi$ is indeed of the postulated form, and retrospectively justifies this assumption.

Denoting $e_v \coloneqq \frac{v}{|v|}$ and $\mu(z) \coloneqq \log(|x| + |y|)$, we find that $\Xi$ is given by
\begin{align*}
  \Xi_z &= - \sigma(z) \cdot (x \cdot |y|^2 + i y \cdot |x|^2) \\
  &= \mu(z) \cdot (e_x \cdot |y| + i e_y \cdot |x|).
\end{align*}


\subsubsection{The Monge-Ampère Flow}

For the curious reader, we write down the flow of the Monge-Ampère / Liouville vector field $\Xi$:
\begin{align*}
  &\psi_\Xi^{t}(z) = e_x \cdot \cosh(e^t \cdot \mu(z)) + i e_y \cdot \sinh(e^t \cdot \mu(z)).
\end{align*}

We will not show that computation here since we are more interested in the flow of the normalized vector field
\begin{align*}
  \xi_x \coloneqq \frac{1}{\mu(z)} \Xi_z = e_x \cdot |y| + i e_y \cdot |x|
\end{align*}
defined away from the zero section $y=0$ instead. We claim that its flow through $z$ for $t \in (-\mu(z),\infty)$ is simply
\begin{align*}
  &\psi_\xi^{t}(z) \coloneqq e_x \cdot \cosh(t + \mu(z)) + i e_y \cdot \sinh(t + \mu(z)).
\end{align*}

To see this we compute:

\begin{align*}
  e_{\Real(\psi_\xi^{t}(z))} &= e_x \frac{\cosh(t + \mu(z))}{|\cosh(t + \mu(z))|} = e_x \\
  e_{\Imag(\phi_\Xi^{t}(z))} &= e_y \frac{\sinh(t + \mu(z))}{|\sinh(t + \mu(z))|} \\
  \xi_{\psi_\xi^{t}(z)} &= e_x \cdot |\sinh(t + \mu(z))| + i e_y \cdot \frac{\sinh(t + \mu(z))}{|\sinh(t + \mu(z))|} \cdot \cosh(t + \mu(z)) \\
  &= \operatorname{sign}(\sinh(t + \mu(z))) \cdot ( e_x \cdot \sinh(t) + i e_y \cdot \cosh(t) ) \\
  &\overset{t + \mu(z) \geq 0}{=} e_x \cdot \sinh(t) + i e_y \cdot \cosh(t) \\
  &= \frac{d}{dt} \psi_\xi^{t}(z)
\end{align*}

Note that for $t + \mu(z) < 0$ the expression $\psi_\xi^{t}(z)$ is the flow of $-\xi$.

Also we claim that the flow of 
\begin{align*}
  J\xi_z = \frac{1}{\mu(z)} J\Xi_z = - e_y \cdot |x| + i e_x \cdot |y|
\end{align*}

is given by

\begin{align*}
  \psi_{J\xi}^{s}(z)
  &= (e_x |x| + i e_y |y|) \cdot \cos(s) - (e_y |x| -i e_x |y|) \cdot \sin(s) \\
  &= v(s) \cdot |x| + i w(s) \cdot |y|
\end{align*}
where we used shorter notation
\begin{align*}
  v(s) &\coloneqq e_x \cdot \cos(s) - e_y \cdot \sin(s) \\
  w(s) &\coloneqq e_x \cdot \sin(s) + e_y \cdot \cos(s).
\end{align*}

Which is just a rotation by $s$ of $e_x, e_y$ (which form an ONB wrt. euklidean distance).

We compute

\begin{align*}
  J\xi_{\psi_{J\xi}^{s}(z)} &= - e_{\Imag(\psi_{J\xi}^{s}(z))} \cdot |\Real(\psi_{J\xi}^{s}(z))| + i e_{\Real(\psi_{J\xi}^{s}(z))} \cdot |\Imag(\psi_{J\xi}^{s}(z))| \\
  &= - w(s) \cdot |x| + i v(s) \cdot |y| = v'(s) \cdot |x| + i w'(s) \cdot |y| \\
  &= \frac{d}{ds} \psi_{J\xi}^{s}(z).
\end{align*}

To get a holomorphic map from this we compose both flows.
For $z = x + iy \in V$ we claim that the map
\begin{align*}
  f_z \colon \set{ t + is \in \mathbb{C} }{ t \neq - \mu(z) } \longrightarrow V,~~ t + i s \longmapsto (\psi_{\xi}^{t} \circ \psi_{J\xi}^{s}) (z)
\end{align*}
extends to a holomorphic map on $\mathbb{C}$. Again for $t < - \mu(z)$ this actually follows the flow of $-\xi$, which will become important when we generalize this procedure in the next section.

We use the identities
\begin{align*}
  \sinh(\sigma) &= \sinh(\Real(\sigma)) \cdot \cos(\Imag(\sigma)) + i \cosh(\Real(\sigma)) \cdot \sin(\Imag(\sigma)) \\
  \cosh(\sigma) &=  \cosh(\Real(\sigma)) \cdot \cos(\Imag(\sigma)) + i \sinh(\Real(\sigma)) \cdot \sin(\Imag(\sigma))
\end{align*}

to see that 
\begin{align*}
  f_z(\sigma = t + i s) &= e_{\Real(\psi_{J\xi}^{s}(z))} \cdot \cosh(t + \mu(\psi_{J\xi}^{s}(z))) \\
  &\hspace{1em}+ i \cdot e_{\Imag(\psi_{J\xi}^{s}(z))} \cdot \sinh(t + \mu(\psi_{J\xi}^{s}(z))) \\
  &= v(s) \cosh(t + \mu(z)) + i w(s) \sinh(t + \mu(z)) \\
  &= (e_x \cdot \cos(s) - e_y \cdot \sin(s)) \cdot \cosh(t + \mu(z)) \\
  &\hspace{1em}+ i \cdot (e_x \cdot \sin(s) + e_y \cdot \cos(s)) \cdot \sinh(t + \mu(z)) \\
  &= e_x \cdot ( \cos(s) \cdot \cosh(t + \mu(z)) + i \sin(s) \cdot \sinh(t + \mu(z)) \\
  &\hspace{1em}+ e_y \cdot ( i \cos(s) \cdot \sinh(t + \mu(z)) - \sin(s) \cosh(t+ \mu(z)) \\
  &= e_x \cosh(t + \mu(z) + is) + i e_y \sinh(t + \mu(z) + i s) \\
  &= e_x \cosh(\sigma + \mu(z)) + i e_y \sinh(\sigma + \mu(z))
\end{align*}

which is clearly homomorphic on $\mathbb{C}$.


\subsection{(Non-)hyperbolicity of Stenzel Complexifications}

So we have seen for complexifications of spheres, that the Monge-Ampère distribution does indeed yield a nonconstant map from the complex plane into the such spaces. Next we will generalize this.

\begin{thm}
  If $M \hookrightarrow X = T^{*}M$ is a Stenzel complexification a compact Riemannian manifold $M$, extending to the whole cotangent bundle, then $X$ is not Brody hyperbolic.

  \begin{proof}
    Denote by $\phi \colon X \rightarrow \mathbb{R}_{\geq 0},~ v \mapsto g(v,v)$ the Kähler potential of $X$ and by $b$ its Kähler metric.
    Now in the setting of Stenzel we have $\Xi = \frac{1}{2} \nabla_b \phi$, since
    \begin{align*}
      b(\Xi,\_) = \alpha(J\_) = - \frac{1}{2} d\phi \circ J \circ J = \frac{1}{2} d\phi.
    \end{align*}
    Also ${|\Xi|_b}^2 = b(\Xi,\Xi) = \frac{1}{2} d\phi(\Xi) = \frac{1}{2} \Xi(\phi) = \phi$. With $u \coloneqq \sqrt{\phi}$, we define the normalized vector field $\xi \coloneqq \frac{1}{u} \cdot \Xi$ away from the zero section and compute for its flow $\psi_{\xi}^{t}$:
    \begin{align*}
      \frac{d}{dt} \phi(\psi_{\xi}^{t}(p)) &= b\Big( {\nabla_b \phi}_{\phi^{t}_{\xi}(p)},~ \frac{d}{dt} \psi_{\xi}^{t}(p)\Big) \\
      &= b\left( 2 u \cdot \xi,\xi \right)_{\psi_{\xi}^{t}(p)} \\
      &= 2 \cdot \sqrt{\phi(\psi_{\xi}^{t}(p))}
    \end{align*}
    Again, away from $0$, this differential equation with initial condition given by $\phi(\psi_{\xi}^{0}(p)) = \phi(p)$ has as its unique solution
    \begin{equation} \label{flowofxi}
      \phi(\psi_{\xi}^{t}(p)) = (t + \phi^{\frac{1}{2}}(p))^2.
    \end{equation}
    Another way to interpret the situation, is to look at $u = \phi^{\frac{1}{2}}$, which is smooth on $X \setminus M$, and observe that $\xi = \nabla_b u$.

    So we conclude that the flow $\psi_{\xi}^t$ through $p \in X$ exists for all times $t \in (-u(p),\infty)$:

    Say to the contrary, that it only exists from time $T_1$ up to time $T_2$, then since $\phi$ vanishes precisely on the zero section, \cref{flowofxi} is valid for all relevant times and the image of the flow curve is contained in $S \coloneqq \phi^{-1}((T_1 + u(p))^2,(T_2 + u(p))^2)$. This set however is precompact, so the flow exists for all times.\\

    The flow of $J\xi$ through $p \in X \setminus M$ is actually contained in the level set $\phi^{-1}(\{\phi(p)\})$, so due to compactness of $M$ its exists for all times.\\

    Next we claim that the flows $\psi_{\xi}$ and $\psi_{J\xi}$ commute:

    From \cite[Lemma 2.4.2]{Stenzel86} we know that $[\Xi,J\Xi] = J\Xi$. Using this identity as well as $J\Xi(\phi) = 0$ and $\Xi(\phi) = 2 \phi$, we compute
    \begin{align*}
      [\xi,J\xi] &= \xi \frac{1}{\sqrt{\phi}} J\Xi - J \xi \frac{1}{\sqrt{\phi}}  \Xi \\
      &= \xi(\frac{1}{\sqrt{\phi}}) \cdot J\Xi + \frac{1}{\phi} \cdot \Xi \circ J \Xi - \left( (J\xi)(\frac{1}{\sqrt{\phi}}) \cdot \Xi + \frac{1}{\phi} \cdot J\Xi \circ \Xi \right) \\
      &= \xi(\frac{1}{\sqrt{\phi}}) \cdot J\Xi + \frac{1}{\phi} [\Xi,J\Xi] \\
      &= \left( \frac{1}{\sqrt{\phi}} \cdot (-\frac{1}{2}) \cdot \phi^{-\frac{3}{2}} \cdot \Xi(\phi) + \frac{1}{\phi} \right) J\Xi = 0
    \end{align*}

    For any $p \in X \setminus M$ we obtain a map
    \begin{align*}
      f_p \colon \mathbb{C} \setminus \mathbb{R} &\longrightarrow X \\
      t + i \cdot s &\longmapsto
      \begin{cases}
        t > 0 \colon& \psi_{\xi}^{t - u(p)} \circ \psi_{J\xi}^s (p) \\
        t < 0 \colon& \psi_{\xi}^{-t - u(p)} \circ \psi_{J\xi}^{-s} (-p)
      \end{cases}
    \end{align*}

    which has as derivatives
    \begin{align*}
      \frac{\partial}{\partial t} f_p(z = t + is) &=
      \begin{cases}
        t > 0 \colon& \xi_{f_p(z)} \\
        t < 0 \colon& -\xi_{f_p(z)}
      \end{cases} \\
      \frac{\partial}{\partial s} f_p(z = t + is) &=
      \begin{cases}
        t > 0 \colon& {J\xi}_{f_p(z)} \\
        t < 0 \colon& {-J\xi}_{f_p(z)}
      \end{cases}.
    \end{align*}
    So this is a holomorphic map, and we claim that it can be extended to the whole complex plane.

    Fix some point $p \in X \setminus M$.

    First we observe that $\lim_{t \searrow 0} \psi_\xi^{t - u(p)}(p)$ exists.
    Take any sequence $t_j \searrow 0$, then $\psi_\xi^{t_j - u(p)}(p)$ is cauchy (wrt. the metric $d_b$ in $X$ induced by the Kähler metric), since
    \begin{align*}
      d_b(\psi_\xi^{s}(p),\psi_\xi^{t}(p)) \leq \left\lvert \int_{s}^{t} {|\xi|}_b \right\rvert = |t - s|.
    \end{align*}
    $M$ was assumed compact, so a small neighbourhood $U \coloneqq \phi^{-1}[0,\epsilon]$ of $M$ will be as well. $U$ is thus complete, and the limit exists as claimed.\\

    So we may extend $f_p$ by setting
    \begin{equation*}
      f_p(0 + i \cdot s) \coloneqq \lim_{t \searrow 0} f_p(t + i \cdot s) = \lim_{t \searrow 0}\psi_{\xi}^{t - u(p)} \circ \psi_{J\xi}^s(p).
    \end{equation*}
    Note that the limit on the right hand side exists since the flow of $J\xi$ preserves $\phi$, so $u(p) = u(\psi_{J\xi}^s(p))$.

    We claim that this choice makes $f_p$ continuous.\\

    To see this, we need to understand the behaviour of the flow under the standard involution $\sigma \colon T^{*}M \rightarrow T^{*}M, p \mapsto -p$. First we investigate the behaviour of $\xi$ under $\sigma$:

    The potential $\phi$ is invariant under $\sigma$, so
    \begin{align*}
      \phi \circ \sigma &= \phi \\
      d\phi_{\sigma(p)} \circ d\sigma_p &= d\phi_p \\
      \alpha_{\sigma(p)} \circ d\sigma_p = - \frac{1}{2} d\phi_{\sigma(p)} \circ J_{\sigma(p)} \circ d\sigma_p &= \frac{1}{2} d\phi_{\sigma(p)} \circ d\sigma_p \circ J_p = \frac{1}{2} d\phi_p \circ J_p = - \alpha_p \\
    \end{align*}
    where we used that $J_{\sigma(p)} \circ d\sigma_p = d\sigma_p \circ J_p$.
    So we have $\sigma^{*}\alpha = - \alpha$, and from this we immediately get
    \begin{align*}
      \sigma^{*} \omega = \sigma^{*} d\alpha = d\sigma^{*}\alpha = - d\alpha = - \omega.
    \end{align*}
    Next since $\sigma$ is an involution, its differential is invertible
    \begin{align*}
      \sigma \circ \sigma &= \operatorname{id} \\
      d\sigma_{\sigma(p)} \circ d\sigma_p &= \operatorname{id}_{T_pX}
    \end{align*}
    and combining these results we obtain
    \begin{align*}
      \omega_{\sigma(p)}(d\sigma_p \xi_p,\_) = \sigma^{*}\omega(\xi_p,(d\sigma_{p})^{-1}\_)= - \omega_p(\xi_p,d\sigma_{\sigma(p)}\_) = - \alpha_p \circ d\sigma_{\sigma(p)} = \alpha_{\sigma(p)}
    \end{align*}
    which implies
    \begin{align*}
      d\sigma_p \xi_p = \xi_{\sigma(p)}
    \end{align*}
    and further gives us
    \begin{align*}
      d\sigma_p {(J\xi)}_p = - J_{\sigma(p)} d\sigma_p \xi_p = - J_{\sigma(p)} \xi_{\sigma(p)}.
    \end{align*}

    Now we claim that for $t \neq 0, s \in \mathbb{R}$ and any $p \in X \setminus M$
    \begin{align}
      &\psi_{J\xi}^{-s}(-p) = (\psi_{J\xi}^{-s} \circ \sigma) (p)
      = \sigma(\psi_{J\xi}^{s}(p)) = - \psi_{J\xi}^{s}(p) \label{invflowjxi} \\
      &\psi_{\xi}^{t-u(p)}(-p) = (\psi_{\xi}^{t-u(p)} \circ \sigma)(p)
      = \sigma(\psi_{\xi}^{t-u(p)}(p)) = - \psi_{\xi}^{t-u(p)}(p). \label{invflowxi}
    \end{align}
    Clearly both sides of these identities agree at $s=0$ and $t = u(p)$ respectively, so we only need to show that they have the same derivative.

    For this we compute
    \begin{align*}
      \frac{d}{ds} (\psi_{J\xi}^{-s} \circ \sigma) (p)
      = - {(J\xi)}_{\sigma(p)}
      &= d\sigma_p {(J\xi)}_p
      = \frac{d}{ds} \sigma(\psi_{J\xi}^{s}(p)) \\
      \frac{d}{dt} (\psi_{\xi}^{t-u(p)} \circ \sigma)(p)
      = \xi_{\sigma(p)}
      &= d\sigma_p \xi_p
      = \frac{d}{dt} \sigma(\psi_{\xi}^{t-u(p)}(p))
    \end{align*}


    Now we can show continuity of $f_p$.
    Take any sequence $t_j \nearrow 0$, then
    \begin{align*}
      f_p(t_j + i \cdot s)
      &\overset{~~~~~~~}{=} \psi_{\xi}^{-t_j - u(p)} \circ \psi_{J\xi}^{-s}(-p) \\
      &\overset{\cref{invflowjxi}}{=} \psi_{\xi}^{-t_j - u(p)}(-\psi_{J\xi}^{s}(p)) \\
      &\overset{\cref{invflowxi}}{=} - \psi_{\xi}^{-t_j - u(p)}(\psi_{J\xi}^{s}(p)) \\
      &\overset{~~~~~~~}{=} - f_p(-t_j + i \cdot s)
    \end{align*}

    So $-t_j \searrow 0$ and $\lim_{t_j \nearrow 0} f_p(t_j + i \cdot s) = \lim_{-t_j \nearrow 0} - f_p(-t_j + i \cdot s) = - f_p(0 + i \cdot s)$, where in the last step we used continuity of the standard involution. But since $f_p(0 + i \cdot s) \in M$ is in the fixed point set of said involution, the limit is the same from both sides.\\


    Next we would like to show smoothness and holomorphy of the extension. We first convince ourselves that the limit $\lim_{t \rightarrow  0} \xi_{f_p(t + i \cdot s)}$ exists:

    Pick a chart centered $(q_j,p_j)$ of $T^{*}M$ around $f(i \cdot s) = \lim_{t \rightarrow  0} f_p(t + i \cdot s) \in M$. In this chart the Liouville vector field $\Xi$ agrees with $\sum_{j=1}^{n}  p_j \frac{\partial}{\partial p_j}$ (where $n=\dim M$), so it points in the direction of the fiber. Its flow curve through $p' \coloneqq \psi_{J\xi}^{s}(p) \in X \setminus M$ is simply

    \begin{align*}
      &\mathbb{R} \longrightarrow T^{*}M \\
      &t \longmapsto e^t \cdot p'.
    \end{align*}

    and the flow of $\xi$ is some reparametrization of this. So the curve $\gamma_s \colon \mathbb{R} \rightarrow X,~ t \mapsto f(t + i \cdot s)$ takes values in the fiber corresponding to $p'$.

    Denote by $|\cdot|_{Eucl.}$ the euklidean norm on $T^{*}_{p'}M$, coming from the euclidean norm in the chart, then clearly $V(s) \coloneqq \left.\frac{\Xi}{|\Xi|_{Eucl.}}\right|_{\gamma_s(t)}$ has a well defined limits
    \begin{align*}
      V_{+} &\coloneqq \lim_{t \searrow 0} V(t) \\
      V_{-} &\coloneqq \lim_{t \nearrow 0} V(t)
    \end{align*}
    In the chart as chosen above, it will actually be locally constant on $\mathbb{R} \setminus \{0\}$.

    The norm $u$ may be viewed as a continuous function on all of $X$, so
    \begin{align*}
      \xi_{\gamma_s(t)} &= \frac{V(t)}{u(V(t))} \overset{t \searrow 0}{\longrightarrow} \frac{V_{+}}{u(V_{+})}
    \end{align*}
    as well as the analogous statement for the limit from the other side, hold.
    Denote by $\sigma$ the standard involution $p \rightarrow -p$ on $X = T^{*}M$, then we have
    \begin{align*}
      &\sigma(\gamma_s(t)) = \gamma_s(-t) \\
      &d\sigma_{\gamma_s(t)}( \left.\frac{d}{dt} \gamma_s\right|_{t} ) = - \left.\frac{d}{dt} \gamma_s\right|_{-t}
    \end{align*}
    so in the limit as $t \searrow 0$ we get
    \begin{align*}
      d\sigma_{\gamma_s(0)}( \lim_{t \searrow 0} \left.\frac{d}{dt} \gamma_s\right|_{t} )
      = - \lim_{t \searrow 0} \left.\frac{d}{dt} \gamma_s\right|_{-t}
    \end{align*}
    and using that in direction of the fiber corresponding to $p'$, $d\sigma_{\gamma_s(0)}$ acts by multiplication with $-1$ we finally see that
    \begin{align*}
      \lim_{t \searrow 0} \left.\frac{d}{dt} \gamma_s\right|_{t}
      = - d\sigma_{\gamma_s(0)}( \lim_{t \searrow 0} \left.\frac{d}{dt} \gamma_s\right|_{t} ) = \lim_{t \searrow 0} \left.\frac{d}{dt} \gamma_s\right|_{-t}
      = \lim_{t \nearrow 0} \left.\frac{d}{dt} \gamma_s\right|_{t}
    \end{align*}

    So $f_p(t + i \cdot s)$ becomes differentiable in $t$ at $t=0$ and the derivative will agree with the limit from either side.

    What remains to be shown, is that $\frac{d}{ds} f_p(0 + i \cdot s) = J \left.\frac{d}{dt}\right|_{t=0} f_p(t + i \cdot s)$ (in particular that it exists at all).
    To see this we rewrite $f_p(0 + i \cdot s) = \lim_{\epsilon \rightarrow 0} \psi_{\xi}^{-\epsilon}(f_p(\epsilon + i \cdot s))$ and compute
    \begin{align*}
      \frac{d}{ds} f_p(0 + i \cdot s) &= \lim_{\epsilon \rightarrow 0} \frac{d}{ds} \psi_{\xi}^{-\epsilon}(f_p(\epsilon + i \cdot s)) \\
      &= \lim_{\epsilon \rightarrow 0} D\psi_{\xi}^{-\epsilon}(\frac{d}{ds} f_p(\epsilon + i \cdot s)) \\
      &= D\psi_{\xi}^{0} (\lim_{\epsilon \rightarrow 0} J \left.\frac{d}{dt}\right|_{t=\epsilon} f_p(t + i \cdot s)) \\
      &= J \left.\frac{d}{dt}\right|_{t=0} f_p(t + i \cdot s)).
    \end{align*}
  So the extension of $f_p$ is $C^1$ and holomorphic at the zero section, and therefore actually smooth, yielding a nonconstant holomorphic map into $X$. This makes the complexification nonhyperbolic.
  \end{proof}
\end{thm}

% todo: | -> \lvert \rvert

